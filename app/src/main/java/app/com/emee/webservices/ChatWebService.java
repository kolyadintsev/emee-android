package app.com.emee.webservices;


import java.util.List;

import app.com.emee.base_classes.ChatMessage;
import app.com.emee.base_classes.PasswordMessage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ChatWebService {
    @GET("/users/findChats")
    Call<List<ChatMessage>> findChats(@Query("requested_id") String requested_id);

    @POST("/message/createChat")
    Call<Long> createChat(@Body ChatMessage chatMessage);
}
