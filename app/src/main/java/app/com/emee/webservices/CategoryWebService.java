package app.com.emee.webservices;

import java.util.List;

import app.com.emee.base_classes.CategoryMessage;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Michael on 20.06.2016.
 */
public interface CategoryWebService {
    @GET("/eventcategories/findAll")
    Call<List<CategoryMessage>> findAll();


}
