package app.com.emee.webservices;

import java.util.List;

import app.com.emee.base_classes.Event;
import app.com.emee.base_classes.EventMessage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Michael on 20.06.2016.
 */
public interface EventWebService {
    @GET("/events/findByUser")
    Call<List<EventMessage>> findByUser(@Query("id") String id);

    @GET("/events/findById")
    Call<EventMessage> findById(@Query("id") String id);

    @POST("/events/createEvent")
    Call<Boolean> createEvent(@Body EventMessage event);

    @GET("/events/findByName")
    Call<List<EventMessage>> findByName(@Query("name") String name);

    @GET("/events/findAll")
    Call<List<EventMessage>> findAll();

    @GET("/events/go")
    Call<Boolean> go (@Query("like_id") String like_id, @Query("user_id") String user_id);

    @GET("/events/like")
    Call<Boolean> like (@Query("like_id") String like_id, @Query("user_id") String user_id);

    @GET("/events/invite")
    Call<Boolean> invite (@Query("like_id") String like_id, @Query("user_id") String user_id);

}
