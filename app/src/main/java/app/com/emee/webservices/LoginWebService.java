package app.com.emee.webservices;

import app.com.emee.base_classes.LoginMessage;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Michael on 16.06.2016.
 */
public interface LoginWebService {
    @GET("login/")
    Call<LoginMessage> loginResult(@Query("login") String login, @Query("password") String password);

}
