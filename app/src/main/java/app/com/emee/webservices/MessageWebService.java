package app.com.emee.webservices;

import java.util.List;

import app.com.emee.base_classes.ChatMessage;
import app.com.emee.base_classes.ConversationMessage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MessageWebService {
    @GET("/message/findMessageById")
    Call<ConversationMessage> findMessageById(@Query("requested_id") String requested_id);

    @GET("/message/findMessagesByChat")
    Call<List<ConversationMessage>> findMessagesByChat(@Query("requested_id") String requested_id);

    @POST("/message/createMessage")
    Call<Boolean> createMessage(@Body ConversationMessage conversationMessage);
}
