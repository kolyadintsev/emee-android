package app.com.emee.webservices;

import java.util.List;

import app.com.emee.base_classes.FeedMessage;
import app.com.emee.base_classes.LoginMessage;
import app.com.emee.base_classes.PasswordMessage;
import app.com.emee.base_classes.User;
import app.com.emee.base_classes.UserMessage;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Michael on 16.06.2016.
 */
public interface UserWebService {
    @GET("/users/findByName")
    Call<List<UserMessage>> findByName(@Query("name") String name, @Query("requested_id") String id);

    @GET("/users/findByLogin")
    Call<UserMessage> findByLogin(@Query("login") String login, @Query("requested_id") String id);

    @GET("/users/findById")
    Call<UserMessage> findById(@Query("id") String user_id, @Query("requested_id") String id);

    @GET("/users/findAll")
    Call<List<UserMessage>> findAll (@Query("requested_id") String request_id);

    @GET("/users/findFeed")
    Call<List<FeedMessage>> findFeed (@Query("requested_id") String request_id);

    @GET("/users/follow")
    Call<Boolean> follow (@Query("like_id") String like_id, @Query("user_id") String user_id);

    @POST("/users/createUser")
    Call<Boolean> createUser(@Body UserMessage userMessage, String password);

    @POST("/users/changePassword")
    Call<Boolean> changePassword(@Body PasswordMessage passwordMessage);
}
