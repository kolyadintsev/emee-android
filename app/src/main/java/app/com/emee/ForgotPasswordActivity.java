package app.com.emee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends Activity {
    @Bind(R.id.editTextSubmitEmail)
    EditText editTextSubmitEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonSubmitEmail)
    public void onClickSubmitEmail (View view) {
        if (CheckEmail()) {
            if (SendMessage()) {
                Toast.makeText(getApplicationContext(), "You have successfully submitted your email", Toast.LENGTH_LONG).show();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(getApplicationContext(), NewPasswordActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "Email submission failed", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please, enter correct Email", Toast.LENGTH_LONG).show();
        }
    }

    private boolean CheckEmail () {
        //TODO change with final logic
        //ONLY DEBUG
        boolean isEmail = editTextSubmitEmail.getText().toString().contains("@") && editTextSubmitEmail.getText().toString().contains(".");
        return isEmail;
    }

    private boolean SendMessage () {
        //TODO change with final logic
        return true;
    }

    public void onNextNP(View view){
        Intent intent = new Intent(this, NewPasswordActivity.class);
        startActivity(intent);
    }
}
