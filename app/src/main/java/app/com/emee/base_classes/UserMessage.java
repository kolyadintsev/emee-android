package app.com.emee.base_classes;

/**
 * Created by Michael on 16.06.2016.
 */
public class UserMessage {
    private long id;
    private String login;
    private String firstName;
    private String lastName;
    private String city;
    private byte age;
    private boolean sex; //true - male, false - female
    private String url;
    private String bio;
    private String photoUrl;
    private boolean isInSearch;
    private boolean isPrivate;
    private long followers;
    private long followings;
    private long events;
    private boolean canFollow;
    private boolean isError;

    public UserMessage(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.city = user.getCity();
        this.age = user.getAge();
        this.sex = user.isSex();
        this.url = user.getUrl();
        this.bio = user.getBio();
        this.photoUrl = user.getPhotoUrl();
        this.isInSearch = user.getIsInSearch();
        this.isPrivate = user.getIsPrivate();
        this.followers = 0;
        this.followings = 0;
        this.canFollow = false;
        this.events = 0;
        isError = false;
    }

    public UserMessage(boolean isError) {
        this.isError = isError;
        this.id = 1;
        this.firstName = "";
        this.lastName = "";
        this.city = "";
        this.age = 0;
        this.sex = false;
        this.url = "";
        this.bio = "";
        this.photoUrl = "";
        this.isInSearch = false;
        this.isPrivate = true;
        this.followers = 0;
        this.followings = 0;
        this.events = 0;
        this.canFollow = false;
        this.isError = isError;

    }

    public UserMessage(long id, String firstName, String lastName, String city, byte age, boolean sex, String url, String bio, String photoUrl, boolean isInSearch, boolean isPrivate, long followers, long followings, long events, boolean canFollow, boolean isError) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.age = age;
        this.sex = sex;
        this.url = url;
        this.bio = bio;
        this.photoUrl = photoUrl;
        this.isInSearch = isInSearch;
        this.isPrivate = isPrivate;
        this.followers = followers;
        this.followings = followings;
        this.events = events;
        this.canFollow = canFollow;
        this.isError = isError;
    }

    public UserMessage(String login, String firstName, String lastName, String city, byte age, boolean sex, String url, String bio, String photoUrl, boolean isInSearch, boolean isPrivate, long followers, long followings, long events, boolean canFollow, boolean isError) {
        this.id = 0;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.age = age;
        this.sex = sex;
        this.url = url;
        this.bio = bio;
        this.photoUrl = photoUrl;
        this.isInSearch = isInSearch;
        this.isPrivate = isPrivate;
        this.followers = followers;
        this.followings = followings;
        this.events = events;
        this.canFollow = canFollow;
        this.isError = isError;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public byte getAge() {
        return age;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isInSearch() {
        return isInSearch;
    }

    public void setInSearch(boolean inSearch) {
        isInSearch = inSearch;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public long getFollowers() {
        return followers;
    }

    public void setFollowers(long followers) {
        this.followers = followers;
    }

    public long getFollowings() {
        return followings;
    }

    public void setFollowings(long followings) {
        this.followings = followings;
    }

    public long getEvents() {
        return events;
    }

    public void setEvents(long events) {
        this.events = events;
    }

    public boolean isCanFollow() {
        return canFollow;
    }

    public void setCanFollow(boolean canFollow) {
        this.canFollow = canFollow;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }
}
