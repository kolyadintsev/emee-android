package app.com.emee.base_classes;

/**
 * Created by Michael on 29.06.2016.
 */
public class Singleton {

    private static Singleton mInstance = null;

    public EventMessage getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(EventMessage eventMessage) {
        this.eventMessage = eventMessage;
    }

    private EventMessage eventMessage;
    private UserMessage userMessage;

    public UserMessage getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(UserMessage userMessage) {
        this.userMessage = userMessage;
    }

    private Singleton(){
    }

    public static Singleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new Singleton();
        }
        return mInstance;
    }


}
