package app.com.emee.base_classes;

import java.util.Date;

/**
 * Created by Michael on 02.08.2016.
 */
public class Message {

private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }


    private User creator;


    private Chat chat;


    private String text;
    private Date date;

    public Message() {
    }

    public Message(User creator, Chat chat, String text, Date date) {
        this.creator = creator;
        this.chat = chat;
        this.text = text;
        this.date = date;
    }
}
