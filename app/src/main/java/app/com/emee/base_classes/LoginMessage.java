package app.com.emee.base_classes;

public class LoginMessage {
    private boolean result;
    private long id;

    public LoginMessage(boolean result, long id) {
        this.result = result;
        this.id = id;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
