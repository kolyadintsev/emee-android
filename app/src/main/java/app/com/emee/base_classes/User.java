package app.com.emee.base_classes;

import java.util.Set;

/**
 * Created by Michael on 16.06.2016.
 */
public class User {

        private long id;

        private String login;

        private String password;

        private String firstName;

        private String lastName;

        private String city;

        private byte age;

        private boolean sex;
        //true - male, false - female

        private String url;

        private String bio;

        private String photoUrl;

        private boolean isInSearch;

        private boolean isPrivate;

        private Set<User> following;

        private Set<User> followers;

        private Set<Event> events;

        private Set<Event> goingEvents;

        private Set<Event> notSureEvents;

        private Set<Event> likesEvents;

        private Set<Comment> comments;

        public User() {
        }

        public User(String login, String password, String firstName, String lastName, String city, byte age, boolean sex, String url, String bio, String photoUrl, boolean isInSearch, boolean isPrivate, Set<User> following, Set<User> followers, Set<Event> events, Set<Event> goingEvents, Set<Event> notSureEvents, Set<Event> likesEvents, Set<Comment> comments) {
            this.login = login;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.city = city;
            this.age = age;
            this.sex = sex;
            this.url = url;
            this.bio = bio;
            this.photoUrl = photoUrl;
            this.isInSearch = isInSearch;
            this.isPrivate = isPrivate;
            this.following = following;
            this.followers = followers;
            this.events = events;
            this.goingEvents = goingEvents;
            this.notSureEvents = notSureEvents;
            this.likesEvents = likesEvents;
            this.comments = comments;
        }

    public User(UserMessage userMessage, String password) {
        this.login = userMessage.getLogin();
        this.password = password;
        this.firstName = userMessage.getFirstName();
        this.lastName = userMessage.getLastName();
        this.city = userMessage.getCity();
        this.age = userMessage.getAge();
        this.sex = userMessage.isSex();
        this.url = userMessage.getUrl();
        this.bio = userMessage.getBio();
        this.photoUrl = userMessage.getPhotoUrl();
        this.isInSearch = userMessage.isInSearch();
        this.isPrivate = userMessage.isPrivate();
        this.following = null;
        this.followers = null;
        this.events = null;
        this.goingEvents = null;
        this.notSureEvents = null;
        this.likesEvents = null;
        this.comments = null;
    }

        public Set<Comment> getComments() {
            return comments;
        }

        public void setComments(Set<Comment> comments) {
            this.comments = comments;
        }

        public void setId(long id) {
            this.id = id;
        }

        public boolean isInSearch() {
            return isInSearch;
        }

        public void setInSearch(boolean inSearch) {
            isInSearch = inSearch;
        }

        public boolean isPrivate() {
            return isPrivate;
        }

        public void setPrivate(boolean aPrivate) {
            isPrivate = aPrivate;
        }

        public Set<Event> getGoingEvents() {
            return goingEvents;
        }

        public void setGoingEvents(Set<Event> goingEvents) {
            this.goingEvents = goingEvents;
        }

        public Set<Event> getNotSureEvents() {
            return notSureEvents;
        }

        public void setNotSureEvents(Set<Event> notSureEvents) {
            this.notSureEvents = notSureEvents;
        }

        public Set<Event> getLikesEvents() {
            return likesEvents;
        }

        public void setLikesEvents(Set<Event> likesEvents) {
            this.likesEvents = likesEvents;
        }

        public long getId() {
            return id;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public byte getAge() {
            return age;
        }

        public void setAge(byte age) {
            this.age = age;
        }

        public boolean isSex() {
            return sex;
        }

        public void setSex(boolean sex) {
            this.sex = sex;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getPhotoUrl() {
            return photoUrl;
        }

        public void setPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
        }

        public boolean getIsInSearch() {
            return isInSearch;
        }

        public void setIsInSearch(boolean isInSearch) {
            this.isInSearch = isInSearch;
        }

        public boolean getIsPrivate() {
            return isPrivate;
        }

        public void setIsPrivate(boolean isPrivate) {
            this.isPrivate = isPrivate;
        }

        public Set<User> getFollowing() {
            return following;
        }

        public void setFollowing(Set<User> following) {
            this.following = following;
        }

        public Set<User> getFollowers() {
            return followers;
        }

        public void setFollowers(Set<User> followers) {
            this.followers = followers;
        }

        public Set<Event> getEvents() {
            return events;
        }

        public void setEvents(Set<Event> events) {
            this.events = events;
        }


}
