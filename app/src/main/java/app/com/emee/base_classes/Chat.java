package app.com.emee.base_classes;

import java.util.Set;

/**
 * Created by Michael on 02.08.2016.
 */
public class Chat {
    private long id;

    private Set<User> users;

    private Set<Message> messages;

    public Chat(Set<User> users, Set<Message> messages) {
        this.users = users;
        this.messages = messages;
    }

    public Chat() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }
}
