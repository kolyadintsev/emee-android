package app.com.emee.base_classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michael on 02.08.2016.
 */
public class ChatMessage {
    private Long id;
    private List<Long> users;
    private List<Long> messages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getUsers() {
        return users;
    }

    public void setUsers(List<Long> users) {
        this.users = users;
    }

    public List<Long> getMessages() {
        return messages;
    }

    public void setMessages(List<Long> messages) {
        this.messages = messages;
    }

    public ChatMessage(Long id, List<Long> users, List<Long> messages) {
        this.id = id;
        this.users = users;
        this.messages = messages;
    }

    public ChatMessage() {
    }

    public ChatMessage(Chat chat) {
        this.id = chat.getId();
        this.users = new ArrayList<Long>();
        this.messages = new ArrayList<Long>();
        for (User user : chat.getUsers()) {
            this.users.add(user.getId());
        }
        for (Message message : chat.getMessages()) {
            this.messages.add(message.getId());
        }
    }
}
