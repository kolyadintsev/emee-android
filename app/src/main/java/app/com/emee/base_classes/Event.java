package app.com.emee.base_classes;

import java.util.Date;
import java.util.Set;

/**
 * Created by Michael on 16.06.2016.
 */
public class Event {
    private long id;

    private User creator;

    private String name;

    private Date startDate;
    private Date endDate;
    private String price;
    private String ageRestrictions;

    private String photoUrl;

    private String description;

    private Set<EventCategory> category;

    private String address;

    private Set<User> going;

    private Set<User> notSure;

    private Set<User> likes;

    private Set<Comment> comments;

    public Event() {
    }

    public Event(User creator, String name, Date startDate, Date endDate, String price, String ageRestrictions, String photoUrl, String description, Set<EventCategory> category, String address, Set<User> going, Set<User> notSure, Set<User> likes, Set<Comment> comments) {
        this.creator = creator;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.ageRestrictions = ageRestrictions;
        this.photoUrl = photoUrl;
        this.description = description;
        this.category = category;
        this.address = address;
        this.going = going;
        this.notSure = notSure;
        this.likes = likes;
        this.comments = comments;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAgeRestrictions() {
        return ageRestrictions;
    }

    public void setAgeRestrictions(String ageRestrictions) {
        this.ageRestrictions = ageRestrictions;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<EventCategory> getCategory() {
        return category;
    }

    public void setCategory(Set<EventCategory> category) {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<User> getGoing() {
        return going;
    }

    public void setGoing(Set<User> going) {
        this.going = going;
    }

    public Set<User> getNotSure() {
        return notSure;
    }

    public void setNotSure(Set<User> notSure) {
        this.notSure = notSure;
    }

    public Set<User> getLikes() {
        return likes;
    }

    public void setLikes(Set<User> likes) {
        this.likes = likes;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

}
