package app.com.emee.base_classes;


import java.util.Date;

public class FeedMessage {
    private UserMessage userMessage;
    private EventMessage eventMessage;
    private Date date;

    public UserMessage getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(UserMessage userMessage) {
        this.userMessage = userMessage;
    }

    public EventMessage getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(EventMessage eventMessage) {
        this.eventMessage = eventMessage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public FeedMessage() {

    }

    public FeedMessage(UserMessage userMessage, EventMessage eventMessage, Date date) {

        this.userMessage = userMessage;
        this.eventMessage = eventMessage;
        this.date = date;
    }

}
