package app.com.emee.base_classes;

import java.util.Set;

/**
 * Created by Michael on 16.06.2016.
 */
public class EventCategory {
    private byte id;

    private String name;

    private Set<Event> events;

    public EventCategory() {
    }

    public EventCategory(String name, Set<Event> events) {
        this.name = name;
        this.events = events;
    }

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }

}
