package app.com.emee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import app.com.emee.base_classes.UserMessage;
import app.com.emee.services.UserService;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpExpandingActivity extends Activity {
    @Bind(R.id.imageViewUserPhotoUpload)
    ImageView userPhotoView;
    @Bind(R.id.buttonRegister)
    Button registerButton;
    @Bind(R.id.editTextFullName)
    EditText editTextFullName;
    @Bind(R.id.editLogin)
    EditText editTextLogin;
    @Bind(R.id.editTextEmail)
    EditText editTextEmail;
    @Bind(R.id.editTextPassword)
    EditText editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_expanding);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.imageViewUserPhotoUpload)
    public void onPhotoUploadClick(View view) {
        //TODO change with final logic
        //ONLY DEBUG
        Toast.makeText(getApplicationContext(), "Do you want to upload your photo?", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.buttonRegister)
    public void onClickRegister (View view){
        //ONLY DEBUG

        /*
        boolean isEmail = editTextEmail.getText().toString().contains("@") && editTextEmail.getText().toString().contains(".");
        if (isEmail && !(editTextFullName.getText().toString().equals("")) && !(editTextEmail.getText().toString().equals("")) &&
                !(editTextPassword.getText().toString().equals("")) && !(editTextLogin.getText().toString().equals(""))) {
            Toast.makeText(getApplicationContext(), "You are registered", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Please, fill all required data", Toast.LENGTH_SHORT).show();
        }
        */
        String[] names = editTextFullName.getText().toString().split(" ", 2);
        UserMessage userMessage = new UserMessage(editTextLogin.getText().toString(), names[0], names[1], "", (byte) 0, true, "", "", "",
                true, false, 0, 0, 0, false, false);
        UserService userService = new UserService();
        userService.createUser(userMessage, editTextPassword.getText().toString(), this);
    }

    public void onNextLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void onSignUpFailure(){
        Toast.makeText(this, "Sign up error. Please, check your credentials.", Toast.LENGTH_LONG).show();
    }


}
