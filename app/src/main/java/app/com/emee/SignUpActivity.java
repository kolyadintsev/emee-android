package app.com.emee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends Activity implements View.OnClickListener {

    @Bind(R.id.buttonRegisterEmail) Button registerEmailButton;
    @Bind(R.id.buttonLogIn) Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    //Method for the processing of OnClick events
    @OnClick({R.id.buttonLogIn, R.id.buttonRegisterEmail})
    public void onClick(View v) {
        Intent intent = null;
        switch(v.getId()){
            case R.id.buttonLogIn :
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                break;
            case R.id.buttonRegisterEmail:
                intent = new Intent(getApplicationContext(), SignUpExpandingActivity.class);
                break;
        }
        startActivity(intent);
    }

    public void onNextSE (View view){
        Intent intent = new Intent(this, SignUpExpandingActivity.class);
        startActivity(intent);
    }
}
