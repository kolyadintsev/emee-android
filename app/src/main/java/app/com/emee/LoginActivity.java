package app.com.emee;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import app.com.emee.base_classes.LoginMessage;
import app.com.emee.services.LoginService;

public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText editPass = (EditText) findViewById(R.id.editPass);
        final EditText editLogin = (EditText) findViewById(R.id.editLogin);
        editPass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!(editLogin.getText().toString().equals("")) && !(editPass.getText().toString().equals(""))){
                    try {
                        Login(editLogin.getText().toString(), editPass.getText().toString());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        editLogin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!(editLogin.getText().toString().equals("")) && !(editPass.getText().toString().equals(""))) {
                    try {
                        Login(editLogin.getText().toString(), editPass.getText().toString());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public  void onLoginClick (View view) throws InterruptedException {
        final EditText editLogin = (EditText) findViewById(R.id.editLogin);
        final EditText editPass = (EditText) findViewById(R.id.editPass);
        if (!(editLogin.getText().toString().equals("")) && !(editPass.getText().toString().equals(""))) {
            Login(editLogin.getText().toString(), editPass.getText().toString());
        }
    }

    public void onForgotPress (View view) {
        EditText loginText = (EditText) findViewById(R.id.editLogin);
        if (!(loginText.getText().toString().equals(""))) {
            Intent intent = new Intent(this, ForgotPasswordActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Please, enter your login", Toast.LENGTH_LONG).show();
        }
    }

    public void onNextFG(View view){
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public void Login (String login, String password) throws InterruptedException {
        LoginService loginService = new LoginService();
        loginService.SendLogin(getApplicationContext(), login, password);
//        LoginMessage loginResult = CheckCredentials(login, password);
//        if (loginResult.isResult() ||(login.equals("test") && password.equals("123"))) {
//            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//            startActivity(intent);
//
//            SharedPreferences sharedPreferences = getSharedPreferences("Emee", MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putLong("Id", loginResult.getId());
//            editor.apply();
//        } else {
//            Toast.makeText(getApplicationContext(), R.string.wrong_pass, Toast.LENGTH_LONG).show();
//        }
    }

//    private LoginMessage CheckCredentials (String login, String password) throws InterruptedException {
//        LoginMessage result = new LoginService().SendLogin(getApplicationContext(), login, password);
//
//        return result;
//    }
}
