package app.com.emee.entities;

import app.com.emee.base_classes.UserMessage;

/**
 * Created by Michael on 19.04.2016.
 */
public class PeopleSearchItem {
    private long id;
    private String title;
    private String ImageLink;
    private boolean FollowState;

    public PeopleSearchItem() {
        this.id = 0;
        this.title = "Nicholas Raymen";
        ImageLink = null;
        FollowState = false;
    }

    public PeopleSearchItem(long id, String title, String imageLink, boolean followState) {
        this.id = id;
        this.title = title;
        ImageLink = imageLink;
        FollowState = followState;
    }

    public PeopleSearchItem(UserMessage userMessage) {
        this.id = userMessage.getId();
        this.title = userMessage.getFirstName() + " "+ userMessage.getLastName();
        ImageLink = userMessage.getPhotoUrl();
        FollowState = userMessage.isCanFollow();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageLink() {
        return ImageLink;
    }

    public void setImageLink(String imageLink) {
        ImageLink = imageLink;
    }

    public boolean isFollowState() {
        return FollowState;
    }

    public void setFollowState(boolean followState) {
        FollowState = followState;
    }
}


