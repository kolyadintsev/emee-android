package app.com.emee.entities;

import app.com.emee.base_classes.EventMessage;

/**
 * Created by Michael on 19.04.2016.
 */
public class EventGridItem {

    private String title;
    private String imageLink;
    private long eventID;

    public EventGridItem(String title, String imageLink, long eventID) {
        this.title = title;
        this.imageLink = imageLink;
        this.eventID = eventID;
    }

    public EventGridItem() {
        this.title = "London conference";
        this.imageLink = null;
        this.eventID = 0;
    }

    public EventGridItem(EventMessage eventMessage) {
        this.title = eventMessage.getName();
        this.imageLink = eventMessage.getPhotoUrl();
        this.eventID = eventMessage.getId();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public long getEventID() {
        return eventID;
    }

    public void setEventID(long eventID) {
        this.eventID = eventID;
    }
}
