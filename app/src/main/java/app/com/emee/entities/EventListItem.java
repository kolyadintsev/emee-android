package app.com.emee.entities;

import java.util.Date;

import app.com.emee.base_classes.EventMessage;

public class EventListItem {
    private String title;
    private String ageRange;
    private String date;
    private String price;
    private String going;
    private String likes;
    private String imageLink;

    public EventListItem(String title, String creator, Integer minimumAge, Date eventDate, Integer price,
                         Integer going, Integer likes, String imageLink) {
        this.title = title + "@" + creator;
        this.ageRange = minimumAge.toString()+"+";
        this.date = eventDate.toString();
        this.price = price.toString()+"$";
        this.going = going.toString() + "people";
        this.likes = likes.toString() + "people";
        this.imageLink = imageLink;
    }

    public EventListItem() {
        //Generate fake data
        this.title = "FASHION NIGHT" + "@" + "MIKSER HOUSE";
        this.ageRange = "18+";
        this.date = "25.04.2016";
        this.price = "50$";
        this.going = "215 people";
        this.likes = "26 people";
        this.imageLink = null;
    }

    public EventListItem(EventMessage event) {
        this.title = event.getName() + "@" + event.getAddress();
        this.ageRange = event.getAgeRestrictions();
//        this.date = event.getStartDate().toString();
        this.date = "test date";
        this.price = event.getPrice();
        this.going = event.getGoing() + " people";
        this.likes = event.getLikes() + " people";
        this.imageLink = event.getPhotoUrl();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGoing() {
        return going;
    }

    public void setGoing(String going) {
        this.going = going;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
