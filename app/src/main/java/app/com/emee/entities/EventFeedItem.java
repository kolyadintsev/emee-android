package app.com.emee.entities;

import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.FeedMessage;
import app.com.emee.base_classes.UserMessage;

public class EventFeedItem {
    private EventListItem item;
    private Long creatorId;
    private String creatorName;
    private String creatorDesc;
    private String creatorImageLink;
    private boolean isPromoted;

    public EventFeedItem(EventListItem item, Long creatorId, String creatorName, String creatorDesc, String creatorImageLink, boolean isPromoted) {
        this.item = item;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
        this.creatorDesc = creatorDesc;
        this.creatorImageLink = creatorImageLink;
        this.isPromoted = isPromoted;
    }

    public EventFeedItem() {
        this.item = new EventListItem();
        this.creatorId = 0L;
        this.creatorName = "TEST USER";
        this.creatorDesc = "is going to an event";
        this.creatorImageLink = null;
        this.isPromoted = false;
    }

    public EventFeedItem(FeedMessage feedMessage) {
        this.item = new EventListItem(feedMessage.getEventMessage());
        this.creatorId = feedMessage.getUserMessage().getId();
        this.creatorName = feedMessage.getUserMessage().getFirstName() + " "+ feedMessage.getUserMessage().getLastName();
        this.creatorDesc = "is going to an event";
        this.creatorImageLink = feedMessage.getEventMessage().getPhotoUrl();
        this.isPromoted = false;
    }

    public EventListItem getItem() {
        return item;
    }

    public void setItem(EventListItem item) {
        this.item = item;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorImageLink() {
        return creatorImageLink;
    }

    public void setCreatorImageLink(String creatorImageLink) {
        this.creatorImageLink = creatorImageLink;
    }

    public boolean isPromoted() {
        return isPromoted;
    }

    public void setPromoted(boolean promoted) {
        isPromoted = promoted;
    }

    public String getCreatorDesc() {
        return creatorDesc;
    }

    public void setCreatorDesc(String creatorDesc) {
        this.creatorDesc = creatorDesc;
    }
}
