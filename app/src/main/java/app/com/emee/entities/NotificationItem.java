package app.com.emee.entities;

import java.util.Date;

import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.FeedMessage;
import app.com.emee.base_classes.UserMessage;

/**
 * Created by Michael on 12.04.2016.
 */
public class NotificationItem {

    private EventListItem item;
    private Long creatorId;
    private String creatorName;
    private String creatorDesc;
    private String action;
    private String creatorImageLink;
    private Date notificationDate;
    private Long eventId;

    public NotificationItem(EventListItem item, Long creatorId, String creatorName, String creatorDesc, String action,
                            String creatorImageLink, Date notificationDate, Long eventId) {
        this.item = item;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
        this.creatorDesc = creatorDesc;
        this.action = action;
        this.creatorImageLink = creatorImageLink;
        this.notificationDate = notificationDate;
        this.eventId = eventId;
    }

    public NotificationItem() {
        this.creatorId = 0L;
        this.creatorName = "TEST USER";
        this.creatorDesc = "is going to";
        this.action = "an event";
        this.creatorImageLink = null;
        this.notificationDate = new Date();
        this.eventId = 0L;
    }

    public NotificationItem(FeedMessage feedMessage) {
        this.creatorId = feedMessage.getUserMessage().getId();
        this.creatorName = feedMessage.getUserMessage().getFirstName() + " "+ feedMessage.getUserMessage().getLastName();
        this.creatorDesc = "is going to";
        this.action = "an event";
        this.creatorImageLink = feedMessage.getUserMessage().getPhotoUrl();
        this.notificationDate = feedMessage.getDate();
        this.eventId = feedMessage.getEventMessage().getId();
    }

    public EventListItem getItem() {
        return item;
    }

    public void setItem(EventListItem item) {
        this.item = item;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCreatorDesc() {
        return creatorDesc;
    }

    public void setCreatorDesc(String creatorDesc) {
        this.creatorDesc = creatorDesc;
    }

    public String getCreatorImageLink() {
        return creatorImageLink;
    }

    public void setCreatorImageLink(String creatorImageLink) {
        this.creatorImageLink = creatorImageLink;
    }

    public Date getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(Date notificationDate) {
        this.notificationDate = notificationDate;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
