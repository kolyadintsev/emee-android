package app.com.emee.entities;

import java.util.Date;

public class ConversationItem {
    private String Text;
    private String Sender;
    private String messageDate;

    public ConversationItem(String text, String sender, String messageDate) {
        Text = text;
        Sender = sender;
        this.messageDate = messageDate;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }
}
