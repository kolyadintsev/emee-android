package app.com.emee.entities;

import java.util.Date;

/**
 * Created by Michael on 20.04.2016.
 */
public class MessageListItem {
    private long conversationID;
    private String message;
    private String imageLink;
    private Date messageDate;
    private String people;

    public MessageListItem(long conversationID, String message, String imageLink, Date messageDate, String people) {
        this.conversationID = conversationID;
        this.message = message;
        this.imageLink = imageLink;
        this.messageDate = messageDate;
        this.people = people;
    }

    public MessageListItem() {
        this.conversationID = 0;
        this.message = "Ok, mate. Will write you later.";
        this.imageLink = null;
        this.messageDate = new Date();
        this.people = "Nick Rhymore";
    }

    public long getConversationID() {
        return conversationID;
    }

    public void setConversationID(long conversationID) {
        this.conversationID = conversationID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }
}
