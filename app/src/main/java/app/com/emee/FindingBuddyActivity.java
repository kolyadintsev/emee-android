package app.com.emee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Michael on 12.04.2016.
 */
public class FindingBuddyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_buddy);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            Thread.sleep(1000);
        } catch (Exception ex) {
            Log.i("Thread sleep error", ex.getMessage());
        }
        FindBuddy();
    }

    public void FindBuddy() {
        //TODO write feature for finding a buddy

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.putExtra("Buddy", 0);
        startActivity(intent);
    }
}
