package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.R;
import app.com.emee.adapters.EventItemAdapter;
import app.com.emee.base_classes.User;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.EventListItem;
import app.com.emee.services.UserService;
import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment {
    @Bind(R.id.profileImageView)
    ImageView imageView;
    @Bind(R.id.profileUserName)
    TextView userName;
    @Bind(R.id.profileUserAge)  TextView userAge;
    @Bind(R.id.profileUserBio) TextView userBio;
    @Bind(R.id.profileUserCity) TextView userCity;
    @Bind(R.id.profileUserDate) TextView userDate;
    @Bind(R.id.profileUserUrl) TextView userUrl;
    @Bind(R.id.profileEvents) TextView profileEvents;
    @Bind(R.id.profileFollowingButton)
    Button profileFollowButton;
    @Bind(R.id.profileFollowing) TextView profileFollowing;
    @Bind(R.id.profileFollowers) TextView profileFollowers;
    @Bind(R.id.profileUpcomingEvents) TextView upcomingEvents;

    Long user_id;
    Long current_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //create fake data
        List<EventListItem> listItems = new ArrayList<EventListItem>();
        listItems.add(new EventListItem());

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);

        ListView listView = (ListView) view.findViewById(R.id.profileListView);
        EventItemAdapter eventItemAdapter = new EventItemAdapter(getContext(), listItems);

//        listView.setAdapter(eventItemAdapter);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getLong("user", 0);
        current_id = sharedPreferences.getLong("Id", 0);

        LoadValues();

        return view;
    }

    public void LoadValues (){
        UserService userService = new UserService();
        userService.findById(user_id, current_id, this);
    }

    public void Init(UserMessage user) {
        Picasso.with(getContext()).load(user.getPhotoUrl()).into(imageView);
        userName.setText(user.getFirstName() + " " + user.getLastName());
        userCity.setText(user.getCity());
//        userAge.setText(user.getAge());
        userBio.setText(user.getBio());
        userUrl.setText(user.getUrl());
        profileEvents.setText("("+user.getEvents()+")");
        profileFollowers.setText("("+user.getFollowers()+")");
        profileFollowing.setText("("+user.getFollowings()+")");
        if (user.isCanFollow()) {
            profileFollowButton.setBackgroundColor(Color.GRAY);
            profileFollowButton.setText("Follow");
            profileFollowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Follow();
                }
            });
        }
        upcomingEvents.setText("UPCOMING EVENTS ("+user.getEvents()+")");
    }

    public void Follow (){
        UserService service = new UserService();
        service.follow(user_id.toString(), current_id.toString(), this);
    }

}
