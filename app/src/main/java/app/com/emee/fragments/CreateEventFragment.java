package app.com.emee.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import app.com.emee.R;
import app.com.emee.base_classes.Event;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.services.EventService;
import butterknife.Bind;
import butterknife.ButterKnife;

public class CreateEventFragment extends Fragment {
    @Bind(R.id.newEventName)
    TextView newEventName;
    @Bind(R.id.newEventAddress) TextView newEventAddress;
    @Bind(R.id.newEventAge) TextView newEventAge;
    @Bind(R.id.newEventDesc) TextView newEventDesc;
    @Bind(R.id.newEventUrl) TextView newEventUrl;
    @Bind(R.id.newEventTime) TextView newEventTime;
    @Bind(R.id.newEventImage)
    ImageView newEventImageView;
    @Bind(R.id.newEventCreate)
    Button eventCreate;
    @Bind(R.id.newEventUploadPhoto) Button uploadPhoto;
    @Bind(R.id.newEventInviteFriends) Button inviteFriends;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event, container, false);
        ButterKnife.bind(this, view);
        eventCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCreateEvent();
            }
        });
        return view;
    }

    private void onCreateEvent () {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        Long user_id = sharedPreferences.getLong("Id", 0);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date date;
        try {
            date = format.parse(newEventTime.getText().toString());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            date = new Date();
        }

        EventMessage eventMessage = new EventMessage(user_id, newEventName.getText().toString(), date,
                "0", newEventAge.getText().toString(), "", newEventDesc.getText().toString(), newEventAddress.getText().toString(), 0L, 0L, 0L);

        EventService service = new EventService();
        service.createEvent(eventMessage);
    }
}
