package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.R;
import app.com.emee.adapters.EventFeedAdapter;
import app.com.emee.base_classes.Event;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.FeedMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.EventFeedItem;
import app.com.emee.entities.EventListItem;
import app.com.emee.services.EventService;
import app.com.emee.services.UserService;

public class NewsFeedFragment  extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        //EventService eventService = new EventService();
        //eventService.findById(2L);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        Long user_id = sharedPreferences.getLong("Id", 0);
        UserService userService = new UserService();
        userService.findFeed(user_id, this);
        return view;
    }

    @Override
    public void onResume() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        Long user_id = sharedPreferences.getLong("Id", 0);
        UserService userService = new UserService();
        userService.findFeed(user_id, this);
        super.onResume();
    }

    public void initUser(List<FeedMessage> feedMessages){
        List<EventFeedItem> listItems = new ArrayList<>();
        for (FeedMessage item : feedMessages) {
            listItems.add(new EventFeedItem(item));
        }
        ListView listView = (ListView) this.getView().findViewById(R.id.feedListView);
        EventFeedAdapter eventFeedAdapter = new EventFeedAdapter(getContext(), listItems);

        listView.setAdapter(eventFeedAdapter);
        this.getView();
    }
}
