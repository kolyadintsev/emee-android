package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.MainActivity;
import app.com.emee.R;
import app.com.emee.adapters.EventGridAdapter;
import app.com.emee.adapters.EventItemAdapter;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.EventGridItem;
import app.com.emee.entities.EventListItem;
import app.com.emee.services.EventService;
import app.com.emee.services.UserService;

public class EventSearchFragment extends Fragment {
    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        LoadValues();

        View view = inflater.inflate(R.layout.fragment_event_search, container, false);

        listView = (ListView) view.findViewById(R.id.eventSearchListView);
        SearchView searchView = (SearchView) view.findViewById(R.id.eventSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Query", s);
                editor.apply();

                LoadValues();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return view;
    }

    private void LoadValues() {
        //List<EventListItem> itemList = new ArrayList<>();

        //Fake
        //itemList.add(new EventListItem());

        //Test
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        String query = sharedPreferences.getString("Query", "");

        EventService eventService = new EventService();

        eventService.findByName(query, this);
        //itemList.add(new EventListItem(Singleton.getInstance().getEventMessage()));

    }

    public void initItems (final List<EventMessage> eventMessages) {
        List<EventListItem> items = new ArrayList<>();
        for (EventMessage event : eventMessages) {
            items.add(new EventListItem(event));
        }

        listView.setAdapter(new EventItemAdapter(getContext(), items));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("Event", eventMessages.get(i).getId());
                editor.apply();

                EventClick();
            }
        });
    }

    private void EventClick() {
        MainActivity activity = (MainActivity) this.getActivity();
        activity.EventClick();
    }

}
