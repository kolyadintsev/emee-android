package app.com.emee.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.FindingBuddyActivity;
import app.com.emee.MainActivity;
import app.com.emee.R;
import app.com.emee.adapters.EventGridAdapter;
import app.com.emee.adapters.EventItemAdapter;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.EventFeedItem;
import app.com.emee.entities.EventGridItem;
import app.com.emee.entities.EventListItem;
import app.com.emee.services.EventService;
import app.com.emee.services.UserService;

public class SearchMainFragment extends Fragment {
    GridView  gridView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        gridView = (GridView) view.findViewById(R.id.searchMainGridView);

        Button button = (Button) view.findViewById(R.id.mainSearchFindBuddyButton);
        EventService eventService = new EventService();
        eventService.findAll(this);

        SearchView searchView = (SearchView) view.findViewById(R.id.mainSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Query", s);
                editor.apply();

                Search();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return view;
    }


    public void initItems (final List<EventMessage> eventMessages) {
        List<EventGridItem> items = new ArrayList<>();
        for (EventMessage event : eventMessages) {
            items.add(new EventGridItem(event));
        }

        gridView.setAdapter(new EventGridAdapter(getContext(), items));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("Event", eventMessages.get(i).getId());
                //editor.putLong("Event", 2);
                editor.apply();

                EventClick();
            }
        });
    }

    private void EventClick() {
        MainActivity activity = (MainActivity) this.getActivity();
        activity.EventClick();
    }

    private void Search() {
        MainActivity activity = (MainActivity) this.getActivity();
        activity.CallSearch();
    }

}
