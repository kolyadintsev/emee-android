package app.com.emee.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.R;
import app.com.emee.adapters.EventItemAdapter;
import app.com.emee.entities.EventListItem;


public class LocationPageFragment extends Fragment{

    private GoogleMap map;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location_page, container, false);

        //Attach the map to FrameLayout
        Fragment fragment = SupportMapFragment.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameMap, fragment);
        ft.commit();

        //create fake data
        List<EventListItem> listItems = new ArrayList<EventListItem>();
        listItems.add(new EventListItem());

        ListView listView = (ListView) view.findViewById(R.id.locationPageEventsListView);
        EventItemAdapter eventItemAdapter = new EventItemAdapter(getContext(), listItems);

        listView.setAdapter(eventItemAdapter);

        return view;
    }
}

