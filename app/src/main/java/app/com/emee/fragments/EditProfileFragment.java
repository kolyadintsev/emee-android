package app.com.emee.fragments;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import app.com.emee.R;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.services.UserService;

public class EditProfileFragment extends Fragment implements View.OnClickListener {

    private EditText name;
    private EditText city;
    private EditText age;
    private EditText web;
    private EditText bio;
    private RadioButton male;
    private RadioButton female;
    private Button save;
    private SwitchCompat emeeSearch;
    private SwitchCompat privateProfile;
    private Button invite;

    private Long user_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);

        //Finding view in XML context
        name = (EditText) view.findViewById(R.id.editProfileName);
        city = (EditText) view.findViewById(R.id.EditProfileCity);
        age = (EditText) view.findViewById(R.id.EditProfileAge);
        web = (EditText) view.findViewById(R.id.EditProfileWeb);
        bio = (EditText) view.findViewById(R.id.EditProfileBio);
        male = (RadioButton) view.findViewById(R.id.EditProfileMale);
        female = (RadioButton) view.findViewById(R.id.EditProfileFemale);
        save = (Button) view.findViewById(R.id.EditProfileSave);
        emeeSearch = (SwitchCompat) view.findViewById(R.id.EditProfileSearch);
        privateProfile = (SwitchCompat) view.findViewById(R.id.EditProfilePrivate);
        invite = (Button) view.findViewById(R.id.EditProfileInvite);

        //Set events listeners
        save.setOnClickListener(this);
        invite.setOnClickListener(this);

        //Load settings from preferences
        loadSettings();

        return view;
    }

    //Load settings from preferences
    private void loadSettings() {
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getLong("Id", 0);

        UserService service = new UserService();
        service.findById(user_id, user_id, this);
        //service.findById(2L, 2L, this);
    }

    //Save settings to preferences
    private void saveSettings() {
        if (name.getText().toString().equals("") || city.getText().toString().equals("") ||
                age.getText().toString().equals("")) {

            Toast.makeText(this.getActivity(), "You should enter the Name, City, Age fields", Toast.LENGTH_SHORT).show();
        }else{
            String[] names = name.getText().toString().split(" ", 2);
            UserMessage userMessage = new UserMessage(user_id, names[0], names[1], city.getText().toString(), Byte.valueOf(age.getText().toString()),
                    male.isChecked(), web.getText().toString(), bio.getText().toString(), "photo_url", emeeSearch.isChecked(),
                    privateProfile.isChecked(), 0, 0, 0, false, false);
            UserService service = new UserService();
            service.createUser(userMessage, this);
        }
    }

    public void Init(UserMessage userMessage) {
        name.setText(userMessage.getFirstName() + " " + userMessage.getLastName());
        city.setText(userMessage.getCity());
//        age.setText(userMessage.getAge());
        web.setText(userMessage.getUrl());
        bio.setText(userMessage.getBio());

        if (userMessage.isSex())
            male.setChecked(true);
        else
            female.setChecked(true);

        emeeSearch.setChecked(userMessage.isInSearch());
        privateProfile.setChecked(userMessage.isPrivate());
    }

    //The event handler
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.EditProfileSave:
                saveSettings();
                break;
            case R.id.EditProfileInvite:
                Toast.makeText(this.getActivity(), "Invite", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
