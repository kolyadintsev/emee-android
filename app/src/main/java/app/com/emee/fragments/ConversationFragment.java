package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import app.com.emee.R;
import app.com.emee.adapters.ConversationListAdapter;
import app.com.emee.base_classes.ConversationMessage;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.ConversationItem;
import app.com.emee.services.ChatService;
import app.com.emee.services.UserService;

public class ConversationFragment extends android.support.v4.app.Fragment {
    ChatService chatService;
    UserService userService;

    Integer size;
    Integer itemsCount = 0;
    Long chat_id = 0L;

    List<ConversationItem> items;
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversation, container, false);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        chat_id = sharedPreferences.getLong("chat", 0);
        chatService = new ChatService();
        userService = new UserService();
        items = new ArrayList<>();

        listView = (ListView) view.findViewById(R.id.conversationListView);

        if (chat_id != 0) {
            chatService.findMessagesByChat(chat_id.toString(), this);
        }

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Reload();
            }
        };
        Timer timer = new Timer();
        timer.schedule(timerTask, 1000);

        return view;
    }

    public void Init(List<ConversationMessage> messageList){
        size = messageList.size();
        for (ConversationMessage message : messageList){
            userService.findById(message.getCreator_id(), 0, message, this);
        }
    }

    public void Reload() {
        chatService.findMessagesByChat(chat_id.toString(), this);
    }

    public void populate(ConversationMessage conversationMessage, UserMessage userMessage) {
        itemsCount ++;
        ConversationItem item = new ConversationItem(conversationMessage.getText(), userMessage.getFirstName() + " " + userMessage.getLastName(),
                conversationMessage.getDate());
        items.add(item);
        if (itemsCount == size) {
            ConversationListAdapter conversationListAdapter = new ConversationListAdapter(getContext(), items);
            listView.setAdapter(conversationListAdapter);
        }
    }

    public void MessageError(){
        Toast.makeText(getContext(), "Message sending error", Toast.LENGTH_LONG).show();
    }

}
