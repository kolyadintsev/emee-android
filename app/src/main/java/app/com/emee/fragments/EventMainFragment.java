package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import app.com.emee.R;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.services.EventService;
import app.com.emee.services.UserService;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Michael on 13.04.2016.
 */
public class EventMainFragment extends Fragment {
    @Bind(R.id.eventTitleTextView) TextView EventTitle;
    @Bind(R.id.eventImageView)
    ImageView EventImageView;
    @Bind(R.id.EventDateItemTextView) TextView EventDate;
    @Bind(R.id.EventAgeItemTextView) TextView EventAge;
    @Bind(R.id.EventItemPriceTextView) TextView EventPrice;
    @Bind(R.id.EventMainLocationCoordTextView) TextView EventLocation;
    @Bind(R.id.EventGoingTextView) TextView EventGoing;
    @Bind(R.id.eventGoingLabelTextView) TextView EventGoingLabel;
    @Bind(R.id.EventMaybeTextView) TextView EventMaybe;
    @Bind(R.id.EventFindBuddyButton)
    Button FindBuddyButton;
    @Bind(R.id.eventMainCreatorImageView) ImageView creatorImageView;
    @Bind(R.id.eventMainCreatorTitleTextView) TextView creatorTitle;
    @Bind(R.id.eventMainCreatorDescTextView) TextView creatorDesc;
    @Bind(R.id.eventMainCommentsListView)
    ListView commentsListView;
    @Bind(R.id.EventAddComment)
    EditText addCommentEditText;
    @Bind(R.id.eventInviteButton)
    Button inviteButton;
    @Bind(R.id.eventLikeButton)
    Button likeButton;

    Long event_id;
    Long user_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_main, container, false);
        ButterKnife.bind(this, view);
        event_id = 0L;
        user_id = 0L;
        EventGoingLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go();
            }
        });

        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invite();
            }
        });

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                like();
            }
        });

        Load();

        return view;
    }

    public void Load() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        event_id = sharedPreferences.getLong("Event", 2);

        EventService service = new EventService();
        service.findById(event_id, this);
    }

    public void initValues(EventMessage event) {
        EventTitle.setText(event.getName());
            Picasso.with(getContext()).load(event.getPhotoUrl()).into(EventImageView);
//            EventDate.setText(event.getStartDate().toString());
            EventAge.setText(event.getAgeRestrictions());
            EventPrice.setText(event.getPrice());
            EventLocation.setText(event.getAddress());
            EventGoing.setText("("+event.getGoing()+")");
            EventMaybe.setText("("+event.getNotSure()+")");

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getLong("Id", 2);

            UserService userService = new UserService();
            userService.findById(user_id, 0, this);
    }

    public void initUser(UserMessage user) {
        if (!user.isError()) {
            Picasso.with(getContext()).load(user.getPhotoUrl()).into(creatorImageView);
            creatorTitle.setText(user.getFirstName() + " " + user.getLastName());
        }
        this.getView();
    }

    public void like () {
        EventService service = new EventService();
        service.like(event_id.toString(), user_id.toString(), this);
    }

    public void go () {
        EventService service = new EventService();
        service.go(event_id.toString(), user_id.toString(), this);
    }

    public void invite () {
        EventService service = new EventService();
        service.invite(event_id.toString(), user_id.toString(), this);
    }

}
