package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.xml.datatype.Duration;

import app.com.emee.MainActivity;
import app.com.emee.R;
import app.com.emee.adapters.MessageListAdapter;
import app.com.emee.base_classes.ChatMessage;
import app.com.emee.base_classes.ConversationMessage;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.MessageListItem;
import app.com.emee.services.ChatService;
import app.com.emee.services.UserService;

/**
 * Created by Michael on 18.04.2016.
 */
public class MessageListFragment extends Fragment {
    ListView listView;
    ChatService chatService;
    UserService userService;
    Long user_id;
    Integer itemCount;
    Integer size;

    List<MessageListItem> listItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_messages, container, false);

        listView = (ListView) view.findViewById(R.id.messagesListView);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getLong("Id", 0);

        listItems = new ArrayList<>();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MainActivity activity = (MainActivity) getActivity();
                activity.ChatClick(listItems.get(i).getConversationID());
            }
        });

        itemCount = 0;

        chatService = new ChatService();
        userService = new UserService();
        chatService.findChats(user_id.toString(), this);
        return view;
    }

    public void Init(List<ChatMessage> list){
        size = list.size();
        for (ChatMessage chatMessage : list) {
            Long max = Collections.max(chatMessage.getMessages());
            chatService.findMessagesById(max.toString(), this);
        }
    }

    public void FindMessages(ConversationMessage conversationMessage) {
        userService.findById(conversationMessage.getCreator_id(), user_id, conversationMessage, this);
    }

    public void populate (ConversationMessage conversationMessage, UserMessage userMessage) {
        itemCount++;

        DateFormat format = new SimpleDateFormat("MM/dd/yyyy hh/mm/ss", Locale.ENGLISH);
        Date date = new Date();
        try {
            date = format.parse(conversationMessage.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        MessageListItem listItem = new MessageListItem(conversationMessage.getChat_id(), conversationMessage.getText(),
                userMessage.getPhotoUrl(), date, userMessage.getFirstName() + " " + userMessage.getLastName());
        listItems.add(listItem);
        if (itemCount == size) {
            MessageListAdapter messageListAdapter = new MessageListAdapter(getContext(), listItems);
            listView.setAdapter(messageListAdapter);
        }
    }


}
