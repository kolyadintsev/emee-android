package app.com.emee.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;

import app.com.emee.R;
import app.com.emee.services.UserService;

public class SearchResultFragment extends Fragment{
    FrameLayout frameLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);

        FragmentTransaction fTrans = getActivity().getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.searchFragmentContainer, new EventSearchFragment());
        fTrans.commit();

        Button eventButton = (Button) view.findViewById(R.id.searchEventsButton);
        Button peopleButton = (Button) view.findViewById(R.id.searchPeopleButton);
        eventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.searchFragmentContainer, new EventSearchFragment());
                fTrans.commit();
            }
        });

        peopleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.searchFragmentContainer, new PeopleSearchFragment());
                fTrans.commit();
            }
        });
        return view;
    }

}
