package app.com.emee.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.MainActivity;
import app.com.emee.R;
import app.com.emee.adapters.NotificationsAdapter;
import app.com.emee.adapters.PeopleSearchAdapter;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.FeedMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.base_classes.User;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.entities.EventFeedItem;
import app.com.emee.entities.NotificationItem;
import app.com.emee.entities.PeopleSearchItem;
import app.com.emee.services.EventService;
import app.com.emee.services.UserService;

/**
 * Created by Michael on 19.04.2016.
 */
public class PeopleSearchFragment extends Fragment {

    UserService userService;
    Long user_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people_search, container, false);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences("Emee", Context.MODE_PRIVATE);
        user_id = sharedPreferences.getLong("Id", 0);
        String query = sharedPreferences.getString("Query", "");

        TextView textView = (TextView) view.findViewById(R.id.peopleSearchStringTextView);
        textView.setText("for " + query);

        userService = new UserService();

        SearchView searchView = (SearchView) view.findViewById(R.id.peopleSearchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                NewSearch(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        userService.findByName(query, user_id, this);
        userService.findById(user_id, user_id, this);

        return view;
    }

    private void NewSearch (String name) {
        userService.findByName(name, user_id, this);
    }

    public void initUser(final List<UserMessage> userMessages){
        List<PeopleSearchItem> listItems = new ArrayList<>();
        for (UserMessage item : userMessages) {
            listItems.add(new PeopleSearchItem(item));
        }
        PeopleSearchAdapter searchAdapter = new PeopleSearchAdapter(getContext(), listItems);
        PeopleSearchAdapter regionAdapter = new PeopleSearchAdapter(getContext(), listItems);

        ListView searchListView = (ListView) this.getView().findViewById(R.id.peopleSearchListView);
        ListView regionListView = (ListView) this.getView().findViewById(R.id.peopleSearchRegionListView);

        searchListView.setAdapter(searchAdapter);
        regionListView.setAdapter(regionAdapter);

        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("user", userMessages.get(i).getId());
                //editor.putLong("Event", 2);
                editor.apply();

                UserClick();
            }
        });

        regionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("user", userMessages.get(i).getId());
                //editor.putLong("Event", 2);
                editor.apply();

                UserClick();
            }
        });

    }

    public void initRegion (final List<UserMessage> userMessages) {
        List<PeopleSearchItem> listItems = new ArrayList<>();
        for (UserMessage item : userMessages) {
            listItems.add(new PeopleSearchItem(item));
        }
        PeopleSearchAdapter regionAdapter = new PeopleSearchAdapter(getContext(), listItems);
        ListView regionListView = (ListView) this.getView().findViewById(R.id.peopleSearchRegionListView);
        regionListView.setAdapter(regionAdapter);
        regionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("Emee", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("user", userMessages.get(i).getId());
                //editor.putLong("Event", 2);
                editor.apply();

                UserClick();
            }
        });

    }

    public void findCity (UserMessage userMessage) {
        userService.findNearby(userMessage.getCity(), user_id, this);
    }

    private void UserClick(){
            MainActivity activity = (MainActivity) this.getActivity();
            activity.EventClick();

    }



}
