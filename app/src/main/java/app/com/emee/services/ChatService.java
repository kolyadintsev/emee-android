package app.com.emee.services;


import java.util.ArrayList;
import java.util.List;

import app.com.emee.MainActivity;
import app.com.emee.base_classes.ChatMessage;
import app.com.emee.base_classes.ConversationMessage;
import app.com.emee.fragments.ConversationFragment;
import app.com.emee.fragments.MessageListFragment;
import app.com.emee.webservices.ChatWebService;
import app.com.emee.webservices.MessageWebService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChatService {
    private String BaseUrl = "http://52.5.194.248:8080/";

    public void findChats (String requested_id, final MessageListFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        ChatWebService service = retrofit.create(ChatWebService.class);
        final Call<List<ChatMessage>> chatMessage = service.findChats(requested_id);
        chatMessage.enqueue(new Callback<List<ChatMessage>>() {
            @Override
            public void onResponse(Call<List<ChatMessage>> call, Response<List<ChatMessage>> response) {
                if (response.isSuccessful()) {
                    fragment.Init(response.body());
                } else {
                    List<ChatMessage> list = new ArrayList<ChatMessage>();
                    list.add(new ChatMessage());
                    fragment.Init(list);
                }
            }

            @Override
            public void onFailure(Call<List<ChatMessage>> call, Throwable t) {
                List<ChatMessage> list = new ArrayList<ChatMessage>();
                list.add(new ChatMessage());
                fragment.Init(list);
            }
        });
    }

    public void findMessagesByChat (String requested_id, final ConversationFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        MessageWebService service = retrofit.create(MessageWebService.class);
        final Call<List<ConversationMessage>> messages = service.findMessagesByChat(requested_id);
        messages.enqueue(new Callback<List<ConversationMessage>>() {
            @Override
            public void onResponse(Call<List<ConversationMessage>> call, Response<List<ConversationMessage>> response) {
                if (response.isSuccessful()) {
                    fragment.Init(response.body());
                } else {
                    List<ConversationMessage> list = new ArrayList<ConversationMessage>();
                    list.add(new ConversationMessage());
                    fragment.Init(list);
                }
            }

            @Override
            public void onFailure(Call<List<ConversationMessage>> call, Throwable t) {
                List<ConversationMessage> list = new ArrayList<ConversationMessage>();
                list.add(new ConversationMessage());
                fragment.Init(list);
            }
        });
    }

    public void findMessagesById (String requested_id, final MessageListFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        MessageWebService service = retrofit.create(MessageWebService.class);
        final Call<ConversationMessage> messages = service.findMessageById(requested_id);
        messages.enqueue(new Callback<ConversationMessage>() {
            @Override
            public void onResponse(Call<ConversationMessage> call, Response<ConversationMessage> response) {
                if (response.isSuccessful()) {
                    fragment.FindMessages(response.body());
                } else {
                    fragment.FindMessages(new ConversationMessage());
                }
            }

            @Override
            public void onFailure(Call<ConversationMessage> call, Throwable t) {
                fragment.FindMessages(new ConversationMessage());
            }
        });
    }


    public void createChat (ChatMessage chatMessage, final MainActivity activity) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        ChatWebService service = retrofit.create(ChatWebService.class);
        Call<Long> result = service.createChat(chatMessage);
        result.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                if (response.isSuccessful()) {
                    activity.ChatCreate(response.body());
                }
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {

            }
        });
    }

    public void createMessenge (ConversationMessage conversationMessage, final ConversationFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
        MessageWebService service = retrofit.create(MessageWebService.class);
        final Call<Boolean> result = service.createMessage(conversationMessage);
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    fragment.Reload();
                } else {
                    fragment.MessageError();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                fragment.MessageError();
            }
        });

    }
}
