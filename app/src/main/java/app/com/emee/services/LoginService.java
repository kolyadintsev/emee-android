package app.com.emee.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;

import app.com.emee.MainActivity;
import app.com.emee.base_classes.LoginMessage;
import app.com.emee.webservices.LoginWebService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Michael on 16.06.2016.
 */
public class LoginService {
    private String BaseUrl = "http://52.5.194.248:8080/";

    public void SendLogin(final Context context, String login, String password) throws InterruptedException {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
// add your other interceptors …

// add logging as last interceptor
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        LoginWebService service = retrofit.create(LoginWebService.class);
        Call<LoginMessage> loginResult = service.loginResult(login, password);
        final LoginMessage[] result = new LoginMessage[1];
        loginResult.enqueue(new Callback<LoginMessage>() {
            @Override
            public void onResponse(Call<LoginMessage> call, Response<LoginMessage> response) {
                Log.e("Login Response", response.body().toString());
                if (response.isSuccessful()) {
                    result[0] = response.body();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    SharedPreferences sharedPreferences = context.getSharedPreferences("Emee", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putLong("Id", response.body().getId());
                    editor.apply();
                    context.startActivity(intent);
                } else {
                    result[0] = new LoginMessage(false, 0);
                }
            }

            @Override
            public void onFailure(Call<LoginMessage> call, Throwable t) {
                Log.d("Error", t.getMessage());
                result[0] = new LoginMessage(false, 0);
            }
        });
    }

}
