package app.com.emee.services;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import app.com.emee.base_classes.Event;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.fragments.EventMainFragment;
import app.com.emee.fragments.EventSearchFragment;
import app.com.emee.fragments.NewsFeedFragment;
import app.com.emee.fragments.NotificationsFragment;
import app.com.emee.fragments.SearchMainFragment;
import app.com.emee.webservices.EventWebService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Michael on 20.06.2016.
 */
public class EventService {
    private String BaseUrl = "http://52.5.194.248:8080/";

    public List<EventMessage> findByUser(Long id){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<List<EventMessage>> eventList = service.findByUser(id.toString());
        try {
            return eventList.execute().body();
        } catch (Exception e) {
            return new ArrayList<EventMessage>();
        }
    }

    public void findById(final Long id){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<EventMessage> eventList = service.findById(id.toString());
        eventList.enqueue(new Callback<EventMessage>() {
            @Override
            public void onResponse(Call<EventMessage> call, Response<EventMessage> response) {
                if (response.isSuccessful()){
                    Singleton.getInstance().setEventMessage(response.body());
                } else {
                    Singleton.getInstance().setEventMessage(new EventMessage());
                }
            }

            @Override
            public void onFailure(Call<EventMessage> call, Throwable t) {
                Singleton.getInstance().setEventMessage(new EventMessage());
            }
        });
    }

    public void findById(Long id, final EventMainFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<EventMessage> eventList = service.findById(id.toString());
        eventList.enqueue(new Callback<EventMessage>() {
            @Override
            public void onResponse(Call<EventMessage> call, Response<EventMessage> response) {
                if (response.isSuccessful()){
                    Log.i("event", response.body().getName());
                    fragment.initValues(response.body());
                } else {
                    fragment.initValues(new EventMessage());
                }
            }

            @Override
            public void onFailure(Call<EventMessage> call, Throwable t) {
                fragment.initValues(new EventMessage());
            }
        });
    }

    public Boolean createEvent(EventMessage event){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<Boolean> result = service.createEvent(event);
        try {
            return result.execute().body();
        } catch (Exception e) {
            return false;
        }
    }

    public void findByName(String name, final EventSearchFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<List<EventMessage>> eventList = service.findByName(name);
        eventList.enqueue(new Callback<List<EventMessage>>() {
            @Override
            public void onResponse(Call<List<EventMessage>> call, Response<List<EventMessage>> response) {
                if (response.isSuccessful()){
                    fragment.initItems(response.body());
                } else {
                    List<EventMessage> list = new ArrayList<EventMessage>();
                    list.add(new EventMessage());
                    fragment.initItems(list);
                }
            }

            @Override
            public void onFailure(Call<List<EventMessage>> call, Throwable t) {
                List<EventMessage> list = new ArrayList<EventMessage>();
                list.add(new EventMessage());
                fragment.initItems(list);
            }
        });

    }

    public void findAll(final SearchMainFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<List<EventMessage>> eventList = service.findAll();
        eventList.enqueue(new Callback<List<EventMessage>>() {
            @Override
            public void onResponse(Call<List<EventMessage>> call, Response<List<EventMessage>> response) {
                if (response.isSuccessful()){
                    fragment.initItems(response.body());
                } else {
                    List<EventMessage> list = new ArrayList<EventMessage>();
                    list.add(new EventMessage());
                    fragment.initItems(list);
                }
            }

            @Override
            public void onFailure(Call<List<EventMessage>> call, Throwable t) {
                List<EventMessage> list = new ArrayList<EventMessage>();
                list.add(new EventMessage());
                fragment.initItems(list);
            }
        });

    }

    public void go (String like_id, String user_id, final EventMainFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<Boolean> result = service.go(like_id, user_id);
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    if (response.body()) {
                        fragment.Load();
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    public void like (String like_id, String user_id, final EventMainFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<Boolean> result = service.go(like_id, user_id);
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    if (response.body()) {
                        fragment.Load();
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    public void invite (String like_id, String user_id, final EventMainFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        EventWebService service = retrofit.create(EventWebService.class);
        Call<Boolean> result = service.go(like_id, user_id);
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    if (response.body()) {
                        fragment.Load();
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

}
