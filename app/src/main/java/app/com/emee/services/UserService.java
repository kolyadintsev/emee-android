package app.com.emee.services;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.com.emee.MainActivity;
import app.com.emee.SignUpExpandingActivity;
import app.com.emee.adapters.PeopleSearchAdapter;
import app.com.emee.base_classes.ConversationMessage;
import app.com.emee.base_classes.EventMessage;
import app.com.emee.base_classes.FeedMessage;
import app.com.emee.base_classes.LoginMessage;
import app.com.emee.base_classes.PasswordMessage;
import app.com.emee.base_classes.Singleton;
import app.com.emee.base_classes.User;
import app.com.emee.base_classes.UserMessage;
import app.com.emee.fragments.ConversationFragment;
import app.com.emee.fragments.EditProfileFragment;
import app.com.emee.fragments.EventMainFragment;
import app.com.emee.fragments.MessageListFragment;
import app.com.emee.fragments.NewsFeedFragment;
import app.com.emee.fragments.NotificationsFragment;
import app.com.emee.fragments.PeopleSearchFragment;
import app.com.emee.fragments.ProfileFragment;
import app.com.emee.webservices.UserWebService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Michael on 17.06.2016.
 */
public class UserService {
    private String BaseUrl = "http://52.5.194.248:8080/";

    public void findByName (String name, Long id, final PeopleSearchFragment fragment){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<List<UserMessage>> userMessage = service.findByName(name, id.toString());
        userMessage.enqueue(new Callback<List<UserMessage>>() {
            @Override
            public void onResponse(Call<List<UserMessage>> call, Response<List<UserMessage>> response) {
                if (response.isSuccessful()) {
                    fragment.initUser(response.body());
                } else {
                    List<UserMessage> list = new ArrayList<UserMessage>();
                    list.add(new UserMessage(true));
                    fragment.initUser(list);
                }
            }

            @Override
            public void onFailure(Call<List<UserMessage>> call, Throwable t) {
                List<UserMessage> list = new ArrayList<UserMessage>();
                list.add(new UserMessage(true));
                fragment.initUser(list);
            }
        });
    }

    public void findNearby (final String city, Long id, final PeopleSearchFragment fragment){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<List<UserMessage>> userMessage = service.findAll(id.toString());
        userMessage.enqueue(new Callback<List<UserMessage>>() {
            @Override
            public void onResponse(Call<List<UserMessage>> call, Response<List<UserMessage>> response) {
                if (response.isSuccessful()) {
                    List<UserMessage> userMessageList = new ArrayList<UserMessage>();
                    for (UserMessage user : response.body()) {
                        if (user.getCity().equals(city)) {
                            userMessageList.add(user);
                        }
                    }
                    fragment.initRegion(userMessageList);
                } else {
                    List<UserMessage> list = new ArrayList<UserMessage>();
                    list.add(new UserMessage(true));
                    fragment.initRegion(list);
                }
            }

            @Override
            public void onFailure(Call<List<UserMessage>> call, Throwable t) {
                List<UserMessage> list = new ArrayList<UserMessage>();
                list.add(new UserMessage(true));
                fragment.initRegion(list);
            }
        });
    }

    public UserMessage findByLogin (String login, long id){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<UserMessage> userMessage = service.findByLogin(login, String.valueOf(id));
        try {
            UserMessage result = userMessage.execute().body();
            return result;
        } catch (Exception e) {
            return new UserMessage(true);
        }
    }

    public void findById (long user_id, long id){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    Singleton.getInstance().setUserMessage(response.body());
                } else {
                    Singleton.getInstance().setUserMessage(new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                Singleton.getInstance().setUserMessage(new UserMessage(true));
            }
        });
    }

    public void findById (long user_id, long id, final ProfileFragment fragment){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    fragment.Init(response.body());
                } else {
                    fragment.Init(new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                fragment.Init(new UserMessage(true));
            }
        });
    }

    public void findById (long user_id, long id, final PeopleSearchFragment fragment){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    fragment.findCity(response.body());
                } else {
                    fragment.findCity(new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                fragment.findCity(new UserMessage(true));
            }
        });
    }

    public void findById (long user_id, long id, final EditProfileFragment fragment){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    fragment.Init(response.body());
                } else {
                    fragment.Init(new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                fragment.Init(new UserMessage(true));
            }
        });
    }

    public void findById (long user_id, long id, final EventMainFragment fragment){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    fragment.initUser(response.body());
                } else {
                    fragment.initUser(new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                fragment.initUser(new UserMessage(true));
            }
        });
    }

    public void findById (long user_id, long id, final ConversationMessage conversationMessage, final MessageListFragment fragment){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    fragment.populate(conversationMessage, response.body());
                } else {
                    fragment.populate(conversationMessage, new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                fragment.populate(conversationMessage, new UserMessage(true));
            }
        });
    }

    public void findById (long user_id, long id, final ConversationMessage conversationMessage, final ConversationFragment fragment){

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<UserMessage> userMessage = service.findById(String.valueOf(user_id), String.valueOf(id));
        userMessage.enqueue(new Callback<UserMessage>() {
            @Override
            public void onResponse(Call<UserMessage> call, Response<UserMessage> response) {
                if (response.isSuccessful()) {
                    fragment.populate(conversationMessage, response.body());
                } else {
                    fragment.populate(conversationMessage, new UserMessage(true));
                }
            }

            @Override
            public void onFailure(Call<UserMessage> call, Throwable t) {
                fragment.populate(conversationMessage, new UserMessage(true));
            }
        });
    }

    public void createUser(UserMessage userMessage, final EditProfileFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<Boolean> result = service.createUser(userMessage, "");
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(fragment.getActivity(), "Saved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(fragment.getActivity(), "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(fragment.getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public Boolean changePassword(PasswordMessage passwordMessage){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<Boolean> result = service.changePassword(passwordMessage);
        try {
            return result.execute().body();
        } catch (Exception e) {
            return false;
        }

    }

    public void findFeed (long id, final NewsFeedFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<List<FeedMessage>> userMessage = service.findFeed(String.valueOf(id));
        userMessage.enqueue(new Callback<List<FeedMessage>>() {
            @Override
            public void onResponse(Call<List<FeedMessage>> call, Response<List<FeedMessage>> response) {
                if (response.isSuccessful()) {
                    fragment.initUser(response.body());
                } else {
                    List<FeedMessage> list = new ArrayList<FeedMessage>();
                    list.add(new FeedMessage());
                    fragment.initUser(list);
                }
            }

            @Override
            public void onFailure(Call<List<FeedMessage>> call, Throwable t) {
                List<FeedMessage> list = new ArrayList<FeedMessage>();
                list.add(new FeedMessage());
                fragment.initUser(list);
            }
        });
    }

    public void findFeed (long id, final NotificationsFragment fragment){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<List<FeedMessage>> userMessage = service.findFeed(String.valueOf(id));
        userMessage.enqueue(new Callback<List<FeedMessage>>() {
            @Override
            public void onResponse(Call<List<FeedMessage>> call, Response<List<FeedMessage>> response) {
                if (response.isSuccessful()) {
                    fragment.initUser(response.body());
                } else {
                    List<FeedMessage> list = new ArrayList<FeedMessage>();
                    list.add(new FeedMessage());
                    fragment.initUser(list);
                }
            }

            @Override
            public void onFailure(Call<List<FeedMessage>> call, Throwable t) {
                List<FeedMessage> list = new ArrayList<FeedMessage>();
                list.add(new FeedMessage());
                fragment.initUser(list);
            }
        });
    }

    public void findAll (long id, final PeopleSearchFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        final Call<List<UserMessage>> userMessage = service.findAll(String.valueOf(id));
        userMessage.enqueue(new Callback<List<UserMessage>>() {
            @Override
            public void onResponse(Call<List<UserMessage>> call, Response<List<UserMessage>> response) {
                if (response.isSuccessful()) {
                    fragment.initUser(response.body());
                } else {
                    List<UserMessage> list = new ArrayList<UserMessage>();
                    list.add(new UserMessage(true));
                    fragment.initUser(list);
                }
            }

            @Override
            public void onFailure(Call<List<UserMessage>> call, Throwable t) {
                List<UserMessage> list = new ArrayList<UserMessage>();
                list.add(new UserMessage(true));
                fragment.initUser(list);
            }
        });
    }

    public void createUser (UserMessage userMessage, String password, final SignUpExpandingActivity activity) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<Boolean> result = service.createUser(userMessage, password);
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    activity.onNextLogin();
                } else {
                    activity.onSignUpFailure();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                activity.onSignUpFailure();
            }
        });
    }

    public void follow (String like_id, String user_id, final ProfileFragment fragment) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        UserWebService service = retrofit.create(UserWebService.class);
        Call<Boolean> result = service.follow(like_id, user_id);
        result.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    if (response.body()) {
                        fragment.LoadValues();
                    }
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }



}
