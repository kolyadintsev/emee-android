package app.com.emee;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import app.com.emee.fragments.ConversationFragment;
import app.com.emee.fragments.CreateEventFragment;
import app.com.emee.fragments.EditProfileFragment;
import app.com.emee.fragments.EventDescriptionFragment;
import app.com.emee.fragments.EventMainFragment;
import app.com.emee.fragments.LocationPageFragment;
import app.com.emee.fragments.MessageListFragment;
import app.com.emee.fragments.NewsFeedFragment;
import app.com.emee.fragments.NotificationsFragment;
import app.com.emee.fragments.ProfileFragment;
import app.com.emee.fragments.SearchMainFragment;
import app.com.emee.fragments.SearchResultFragment;
import app.com.emee.services.EventService;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ProfileFragment profileFragment;
    //private EventDescriptionFragment eventDescription;
    private NewsFeedFragment newsFeedFragment;
    private NotificationsFragment notificationsFragment;
    private LocationPageFragment locationPage;
    private FragmentTransaction fTrans;
    private MessageListFragment messageListFragment;
    private SearchMainFragment searchMainFragment;
    private ImageButton profile;
    private ImageButton feed;
    private ImageButton home;
    private ImageButton notify;
    private ImageButton messages;

    public long getUser_id() {
        return user_id;
    }

    private long user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        initToolBar();

        SharedPreferences sharedPreferences = getSharedPreferences("Emee", MODE_PRIVATE);
        user_id = sharedPreferences.getLong("Id", 0);

        int buddy = 0;
        try {
            buddy = getIntent().getIntExtra("Buddy", 0);
        } catch (Exception ex) {
            Log.i("MainActivityInfo", "Buddy id not found");
        }

        if (buddy != 0){
            //TODO call find buddy function
        }
    }

    //Finding views, set listeners and so on
    private void init() {
        profile = (ImageButton) findViewById(R.id.profile);
        feed = (ImageButton) findViewById(R.id.feed);
        home = (ImageButton) findViewById(R.id.home);
        notify = (ImageButton) findViewById(R.id.notify);
        messages = (ImageButton) findViewById(R.id.messages);

        profile.setOnClickListener(this);
        feed.setOnClickListener(this);
        home.setOnClickListener(this);
        notify.setOnClickListener(this);
        messages.setOnClickListener(this);

        //eventDescription = new EventDescriptionFragment();
        locationPage = new LocationPageFragment();
        profileFragment = new ProfileFragment();
        newsFeedFragment = new NewsFeedFragment();
        notificationsFragment = new NotificationsFragment();
        messageListFragment = new MessageListFragment();
        searchMainFragment = new SearchMainFragment();

        //Show first fragment

        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont,searchMainFragment);
        fTrans.commit();
    }

    //Set Action Bar
    private void initToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_edit_profile:
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.frgmCont, new EditProfileFragment());
                fTrans.commit();
                return true;
            case R.id.menu_create_event:
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.frgmCont, new CreateEventFragment());
                fTrans.commit();
                return true;
            case R.id.menu_adds:
                //TODO ADDS SCREEN
                return true;
            case R.id.menu_about:
                //TODO ABOUT SCREEN
                return true;
            case R.id.menu_log_out:
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);

                SharedPreferences sharedPreferences = getSharedPreferences("Emee", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("Id", 0);
                editor.apply();

                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        fTrans = getSupportFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.profile:
                SharedPreferences sharedPreferences = this.getSharedPreferences("Emee", Context.MODE_PRIVATE);
                Long user_id = sharedPreferences.getLong("Id", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("user", user_id);
                editor.apply();
                fTrans.replace(R.id.frgmCont, profileFragment); //replacing fragment on main activity_main
                break;
            case R.id.feed:
                fTrans.replace(R.id.frgmCont, newsFeedFragment);
                break;
            case R.id.home:
                fTrans.replace(R.id.frgmCont, searchMainFragment);
                EventService eventService = new EventService();
                eventService.findAll(searchMainFragment);
                break;
            case R.id.notify:
                fTrans.replace(R.id.frgmCont, notificationsFragment);
                break;
            case R.id.messages:
                fTrans.replace(R.id.frgmCont, messageListFragment);
                break;
        }
        fTrans.commit();
    }

    public void onFindBuddyClick(View view) {
        Intent intent = new Intent(getApplicationContext(), FindingBuddyActivity.class);
        startActivity(intent);
    }

    public void EventClick(){
        //TEMP
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, new EventMainFragment());
        fTrans.commit();
    }

    public void UserClick(){
        //TEMP
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, new ProfileFragment());
        fTrans.commit();
    }

    public void ChatCreate(Long chat_id) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("Emee", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("chat", chat_id);
        editor.apply();

        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, new ConversationFragment());
        fTrans.commit();
    }

    public void ChatClick(Long chat_id){
        SharedPreferences sharedPreferences = this.getSharedPreferences("Emee", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("chat", chat_id);
        editor.apply();

        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, new ConversationFragment());
        fTrans.commit();
    }

    public void CallSearch() {
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.frgmCont, new SearchResultFragment());
        fTrans.commit();
    }
}
