package app.com.emee.adapters;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.com.emee.R;
import app.com.emee.entities.ConversationItem;

public class ConversationListAdapter extends ArrayAdapter<ConversationItem> {

    private List<ConversationItem> items;
    private final Context context;

    public ConversationListAdapter(Context context, List<ConversationItem> objects) {
        super(context, R.layout.item_message, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_conversation, parent, false);

        TextView messageText = (TextView) rowView.findViewById(R.id.messageTextView);
        TextView senderText = (TextView) rowView.findViewById(R.id.senderTextView);
        TextView sendTime = (TextView) rowView.findViewById(R.id.sendTimeTextView);
        LinearLayout linearLayout = (LinearLayout) rowView.findViewById(R.id.senderDetailsLayout);

        ConversationItem conversationItem = items.get(position);
        messageText.setText(conversationItem.getText());
        senderText.setText(conversationItem.getSender());
        sendTime.setText(conversationItem.getMessageDate());

        if (!conversationItem.getSender().equals("You")) {
           LinearLayout.LayoutParams layoutParams  = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,0,60,0);
            messageText.setLayoutParams(layoutParams);
            messageText.setBackgroundColor(Color.TRANSPARENT);
            linearLayout.setLayoutParams(layoutParams);
        }

        return rowView;
    }
}
