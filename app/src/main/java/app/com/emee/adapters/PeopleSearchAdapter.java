package app.com.emee.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import app.com.emee.R;
import app.com.emee.entities.PeopleSearchItem;


public class PeopleSearchAdapter extends ArrayAdapter<PeopleSearchItem> {


    private List<PeopleSearchItem> items;
    private final Context context;

    public PeopleSearchAdapter(Context context, List<PeopleSearchItem> objects) {
        super(context, R.layout.fragment_people_search, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_people_search, parent, false);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.peopleSearchItemImageView);
        TextView textView = (TextView) rowView.findViewById(R.id.peopleSearchItemTextView);

        textView.setText(items.get(position).getTitle());

        if (items.get(position).getImageLink() != null) {
            Picasso.with(context).load(Uri.parse(items.get(position).getImageLink())).into(imageView);
        } else {
            Picasso.with(context).load(R.drawable.logo_emee).into(imageView);
        }

        return rowView;
    }
}
