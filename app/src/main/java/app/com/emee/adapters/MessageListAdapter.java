package app.com.emee.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.com.emee.R;
import app.com.emee.entities.MessageListItem;

/**
 * Created by Michael on 20.04.2016.
 */
public class MessageListAdapter extends ArrayAdapter<MessageListItem> {

    private List<MessageListItem> items;
    private final Context context;

    public MessageListAdapter(Context context, List<MessageListItem> objects) {
        super(context, R.layout.item_message, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_message, parent, false);

        TextView messageText = (TextView) rowView.findViewById(R.id.messageListItemTextView);
        TextView peopleText = (TextView) rowView.findViewById(R.id.messageListItemContactsTextView);
        TextView dateView = (TextView) rowView.findViewById(R.id.messageListItemTimeTextView);
        ImageView creatorImage = (ImageView) rowView.findViewById(R.id.messageListItemImageView);

        Date now = new Date();
        long term = now.getTime() - items.get(position).getMessageDate().getTime();
        long[] time = {TimeUnit.MILLISECONDS.toDays(term), TimeUnit.MILLISECONDS.toHours(term),
                TimeUnit.MILLISECONDS.toMinutes(term), TimeUnit.MILLISECONDS.toSeconds(term)};
        String dateText = "";

        for (int i = 0; i < time.length; i++){
            Long result = time[i];
            if (result >= 1) {
                switch (i) {
                    case 0:
                        dateText = result.toString() + " days ago";
                        break;
                    case 1:
                        dateText = result.toString() + " hours ago";
                        break;
                    case 2:
                        dateText = result.toString() + " minutes ago";
                        break;
                    case 3:
                        dateText = result.toString() + " seconds ago";
                        break;
                }
            }
        }

        messageText.setText(items.get(position).getMessage());
        peopleText.setText(items.get(position).getPeople());
        dateView.setText(dateText);

        if (items.get(position).getImageLink() != null) {
            Picasso.with(context).load(Uri.parse(items.get(position).getImageLink())).into(creatorImage);
        } else {
            Picasso.with(context).load(R.drawable.logo_emee).into(creatorImage);
        }

        return rowView;
    }
}
