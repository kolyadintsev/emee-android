package app.com.emee.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.emee.R;
import app.com.emee.entities.EventGridItem;

/**
 * Created by Michael on 19.04.2016.
 */
public class EventGridAdapter extends ArrayAdapter<EventGridItem> {

    private List<EventGridItem> itemList;
    private final Context context;

    public EventGridAdapter(Context context, List<EventGridItem> objects) {
        super(context, R.layout.item_event_grid, objects);
        this.context = context;
        this.itemList = objects;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_event_grid, parent, false);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.eventGridItemImageView);
        TextView textView = (TextView) rowView.findViewById(R.id.eventGridItemTitleTextView);

        textView.setText(itemList.get(position).getTitle());

        if (itemList.get(position).getImageLink() != null) {
            Picasso.with(context).load(Uri.parse(itemList.get(position).getImageLink())).into(imageView);
        } else {
            Picasso.with(context).load(R.drawable.logo_emee).into(imageView);
        }

        return rowView;
    }
}
