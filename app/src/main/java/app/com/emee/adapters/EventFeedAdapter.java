package app.com.emee.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.emee.R;
import app.com.emee.entities.EventFeedItem;
import app.com.emee.entities.EventListItem;

/**
 * Created by Michael on 12.04.2016.
 */
public class EventFeedAdapter extends ArrayAdapter<EventFeedItem> {

    private List<EventFeedItem> feedList;
    private final Context context;

    public EventFeedAdapter(Context context, List<EventFeedItem> objects) {
        super(context, R.layout.item_event_feed, objects);
        this.feedList = objects;
        this.context = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_event_feed, parent, false);

        TextView creatorTitleView = (TextView) rowView.findViewById(R.id.eventFeedCreatorTitleTextView);
        TextView creatorTitleDescView = (TextView) rowView.findViewById(R.id.eventFeedTitleDescTextView);
        ImageView creatorImage = (ImageView) rowView.findViewById(R.id.eventFeedCreatorImageView);

        TextView titleView = (TextView) rowView.findViewById(R.id.EventTitleItemTextView);
        TextView dateView = (TextView) rowView.findViewById(R.id.EventDateItemTextView);
        TextView ageView = (TextView) rowView.findViewById(R.id.EventAgeItemTextView);
        TextView priceView = (TextView) rowView.findViewById(R.id.EventItemPriceTextView);
        TextView goingView = (TextView) rowView.findViewById(R.id.EventGoingItemTextView);
        TextView likesView = (TextView) rowView.findViewById(R.id.EventLikeItemTextView);
        ImageView eventImage = (ImageView) rowView.findViewById(R.id.EventItemImageView);

        creatorTitleView.setText(feedList.get(position).getCreatorName());
        creatorTitleDescView.setText(feedList.get(position).getCreatorDesc());
        if (feedList.get(position).getCreatorImageLink() != null) {
            Picasso.with(context).load(Uri.parse(feedList.get(position).getCreatorImageLink())).into(creatorImage);
        } else {
            Picasso.with(context).load(R.drawable.logo_emee).into(creatorImage);
        }

        EventListItem item = feedList.get(position).getItem();

        titleView.setText(item.getTitle());
        dateView.setText(item.getDate());
        ageView.setText(item.getAgeRange());
        priceView.setText(item.getPrice());
        goingView.setText(item.getGoing());
        likesView.setText(item.getLikes());

        if (item.getImageLink() != null) {
            Picasso.with(context).load(Uri.parse(item.getImageLink())).into(eventImage);
        } else {
            Picasso.with(context).load(R.drawable.logo_emee).into(eventImage);
        }

        return rowView;
    }
}
