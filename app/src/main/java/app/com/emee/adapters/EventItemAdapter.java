package app.com.emee.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.emee.R;
import app.com.emee.entities.EventListItem;

public class EventItemAdapter extends ArrayAdapter<EventListItem> {
    private List<EventListItem> itemsArrayList;
    private final Context context;

    public EventItemAdapter(Context context, List<EventListItem> objects) {
        super(context, R.layout.item_event_search, objects);
        this.itemsArrayList = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_event_search, parent, false);

        TextView titleView = (TextView) rowView.findViewById(R.id.EventTitleItemTextView);
        TextView dateView = (TextView) rowView.findViewById(R.id.EventDateItemTextView);
        TextView ageView = (TextView) rowView.findViewById(R.id.EventAgeItemTextView);
        TextView priceView = (TextView) rowView.findViewById(R.id.EventItemPriceTextView);
        TextView goingView = (TextView) rowView.findViewById(R.id.EventGoingItemTextView);
        TextView likesView = (TextView) rowView.findViewById(R.id.EventLikeItemTextView);
        ImageView eventImage = (ImageView) rowView.findViewById(R.id.EventItemImageView);

        titleView.setText(itemsArrayList.get(position).getTitle());
        dateView.setText(itemsArrayList.get(position).getDate());
        ageView.setText(itemsArrayList.get(position).getAgeRange());
        priceView.setText(itemsArrayList.get(position).getPrice());
        goingView.setText(itemsArrayList.get(position).getGoing());
        likesView.setText(itemsArrayList.get(position).getLikes());

        if (itemsArrayList.get(position).getImageLink() != null) {
            Picasso.with(context).load(Uri.parse(itemsArrayList.get(position).getImageLink())).into(eventImage);
        } else {
            Picasso.with(context).load(R.drawable.logo_emee).into(eventImage);
        }

        return rowView;

    }
}
