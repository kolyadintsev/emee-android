package app.com.emee.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.com.emee.R;
import app.com.emee.entities.NotificationItem;

/**
 * Created by Michael on 12.04.2016.
 */
public class NotificationsAdapter extends ArrayAdapter<NotificationItem> {
    private List<NotificationItem> items;
    private final Context context;

    public NotificationsAdapter(Context context, List<NotificationItem> objects) {
        super(context, R.layout.item_notification, objects);
        this.items = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.item_notification, parent, false);

        TextView creatorTitleView = (TextView) rowView.findViewById(R.id.notificationCreatorTitleTextView);
        TextView creatorTitleDescView = (TextView) rowView.findViewById(R.id.notificationTitleDescTextView);
        TextView actionView = (TextView) rowView.findViewById(R.id.notificationActionTextView);
        TextView dateView = (TextView) rowView.findViewById(R.id.notificationDateTextView);
        ImageView creatorImage = (ImageView) rowView.findViewById(R.id.notificationImageView);
if (items.get(position) != null) {
    /*Date now = new Date();
    long term = now.getTime() - items.get(position).getNotificationDate().getTime();
    long[] time = {TimeUnit.MILLISECONDS.toDays(term), TimeUnit.MILLISECONDS.toHours(term),
            TimeUnit.MILLISECONDS.toMinutes(term), TimeUnit.MILLISECONDS.toSeconds(term)};
    String dateText = "";

    for (int i = 0; i < time.length; i++) {
        Long result = time[i];
        if (result >= 1) {
            switch (i) {
                case 0:
                    dateText = result.toString() + " days ago";
                    break;
                case 1:
                    dateText = result.toString() + " hours ago";
                    break;
                case 2:
                    dateText = result.toString() + " minutes ago";
                    break;
                case 3:
                    dateText = result.toString() + " seconds ago";
                    break;
            }
        }
    }*/

    creatorTitleView.setText(items.get(position).getCreatorName());
    creatorTitleDescView.setText(items.get(position).getCreatorDesc());
    actionView.setText(items.get(position).getAction());
    //dateView.setText(dateText);

    if (items.get(position).getCreatorImageLink() != null) {
        Picasso.with(context).load(Uri.parse(items.get(position).getCreatorImageLink())).into(creatorImage);
    } else {
        Picasso.with(context).load(R.drawable.logo_emee).into(creatorImage);
    }
}

        return rowView;
    }



}
